const express = require('express');
const next = require('next');
const bodyParser = require('body-parser');

const dev = process.env.NODE_ENV !== 'production';
const app = next({ dev });
const handle = app.getRequestHandler();
const port = process.env.PORT || 3002;
const expressServer = express();

app.prepare()
.then(() => {
    expressServer.use(bodyParser.json());
    expressServer.get('/ping', (req, res) => res.send('Hey there.'));
    expressServer.get('/', (req, res) => {
      return app.render(req, res, '/')
    })
    expressServer.get('/aboutUs', (req, res) => {
      return app.render(req, res, '/aboutUs')
    })
    expressServer.get('/contact', (req, res) => {
      return app.render(req, res, '/contact')
    })
    expressServer.get('*', (req, res) => {
      return handle(req, res);
    });
    expressServer.listen(3002, err => {
      if(err) throw err;
      console.log('Ready on: ' + port + ' using express');
    });
  })
  .catch(err => {
    console.log(err);
    console.error(err);
    process.exit(1);
  });