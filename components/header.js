import React from 'react';
import Link from 'next/link';
import { Layout, Row, Col, Icon } from 'antd/lib';

const { Header, Footer, Sider, Content } = Layout;

const CommonHeader = () => {
  const handleMenu = e => {
    e.preventDefault();
    if( document.getElementById("home-menu").classList.contains('header-home-menu')) {
        document.getElementById("home-menu").classList.remove('header-home-menu');
        document.getElementById("home-menu").classList.add('non-header-home-menu');
    } else {
        document.getElementById("home-menu").classList.remove('non-header-home-menu');
        document.getElementById("home-menu").classList.add('header-home-menu');
    }
  }
  return (
    <Header style={{background: 'white',height: 'fit-content'}} className="normal-header">
      <Row>
        <Col xs={12} sm={12} md={0} lg={0}><Link href="/"><span className="header-title">Cargo</span></Link></Col>
        <Col xs={12} sm={12} md={0} lg={0}  className="mobile-menu-icon"><Icon onClick={handleMenu} className="menu-icon" type="menu-fold" /></Col>
      </Row>
      <Row>
        <Col xs={0} sm={0} md={7} lg={7}><Link href="/"><span className="header-title">Cargo</span></Link></Col>
        <Col xs={24} sm={24} md={17} lg={17}>
          <Row type="flex" justify="end" className="home-menu" id="home-menu">
            <Col xs={24} sm={24} md={4} lg={4}  className="sub-menu"><Link href="/drive">Drive</Link></Col>
            <Col xs={24} sm={24} md={4} lg={4}  className="sub-menu">Send</Col>
            <Col xs={24} sm={24} md={4} lg={4}  className="sub-menu">Track</Col>
            <Col xs={24} sm={24} md={4} lg={4}  className="sub-menu">Careers</Col>
            <Col xs={24} sm={24} md={4} lg={4}  className="sub-menu"><Link href="/support">Support</Link></Col>
            <Col xs={24} sm={24} md={4} lg={4}  className="sub-menu">Signup<Icon type="arrow-right" /></Col>
          </Row>
        </Col>
      </Row>
    </Header>
  )
}

export default CommonHeader;