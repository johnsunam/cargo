import React from 'react';
import Link from 'next/link';
import { Layout, Row, Col, Icon } from 'antd/lib';

const { Header, Footer, Sider, Content } = Layout;

const CommonFooter = () => {
  return (
    // <Footer>
      <div className="footer">
        <Row  style={{marginBottom: '5%'}}>
          <Col sm={8} lg={6} className="footer-sub">
            <ul className="footer-menu">
              <li className="footer-head"><span>Resources</span></li>
              <li className="footer-list"><span>Training Materials</span></li>
            </ul>
          </Col>
          <Col sm={8} lg={6} className="footer-sub">
            <ul className="footer-menu">
              <li className="footer-head"><span>Company</span></li>
              <li className="footer-list"><span>Careers</span></li>
              <li className="footer-list"><span>Blog</span></li>
            </ul>
          </Col>
          <Col sm={8} lg={6} className="footer-sub">
            <ul className="footer-menu">
              <li className="footer-head"><Link href="/aboutUs"><span>About Us</span></Link></li>
              <li className="footer-list"><Link href="/contact"><span>Contact</span></Link></li>
              <li className="footer-list"><span>Press</span></li>
            </ul>
          </Col>
          <Col sm={24} lg={6} className="footer-social">
            <span className="footer-head">Get the app</span>
            <div className="app-store">
              <div style={{textAlign: 'center'}}>
                <div className="app-icon">
                  <Icon type="apple" />
                </div>
                <div>Get it on</div>
                <div className="app-header">App Store</div>
              </div>
              <div style={{marginLeft: '10%', textAlign: 'center'}}>
                <div className="app-icon">
                  <Icon type="android" />
                </div>
                <div>Get it on</div>
                <div className="app-header">Google Play</div>
              </div>
            </div>
          </Col>
        </Row>
        <hr />
        <div style={{display: 'flex', flexWrap: 'nowrap', marginTop: 33}}>
          <div > <span className="media"><Icon type="facebook" /></span></div>
          <div > <span className="media"><Icon type="twitter" /></span></div>
          <div > <span className="media"><Icon type="instagram" /></span></div>
          <div > <span className="media"><Icon type="linkedin" /></span></div>
          <div  className="copyright"> <span>All rights reserved</span></div>
        </div>
      </div>
    // </Footer>
  )
}

export default CommonFooter;