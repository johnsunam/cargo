import React from 'react';
import { Layout, Row, Col} from 'antd/lib';
import CommonHeader from './header';
import CommonFooter from './footer';
const { Content } = Layout;

const CommonLayout = props => {
  return (
    <div>
      <Layout>
        <CommonHeader/>
        <Content>
          {props.children}
        </Content>
        <CommonFooter/>
      </Layout>
    </div>
  )
}

export default CommonLayout;