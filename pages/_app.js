import React from 'react';
import { Provider } from 'react-redux';
import App from 'next/app';
import withRedux from 'next-redux-wrapper';
import withReduxSaga from 'next-redux-saga';
import Router from 'next/router';
import NProgress from 'nprogress';
import createStore from '../store';
import 'antd/dist/antd.css';
import '../public/static/css/style.scss';
import '../public/static/css/ngprogress.css';

Router.onRouteChangeStart = () => {
  NProgress.start();
}
Router.onRouteChangeComplete = () => {
  NProgress.done();
}

Router.onRouteChangeError = () => {
  NProgress.done();
};



class MyApp extends App {
  static async getInitialProps({ Component, ctx}) {
    let pageProps = {};
    if(Component.getInitialProps) {
      pageProps = await Component.getInitialProps({ ctx });

    }
    return { pageProps };
  }
  componentDidMount() {
    document.addEventListener("scroll", function(){
      if(screen.width > 768) {
        const imageScroller = document.getElementById('image-scroller');
        const firstItem = document.getElementById('first');
        const secondItem = document.getElementById('second');
        const thirdItem = document.getElementById('third');
        const lastItem = document.getElementById('forth');
        const videoScroller = document.getElementById('video-carousel');
        if(imageScroller && firstItem && secondItem && thirdItem && lastItem && videoScroller) {
          var imageBounding = imageScroller.getBoundingClientRect()
          var firstBounding = firstItem.getBoundingClientRect();
          var secondBounding = secondItem.getBoundingClientRect();
          var thirdBounding = thirdItem.getBoundingClientRect();
          var lastBounding = lastItem.getBoundingClientRect();
          var videoBounding = videoScroller.getBoundingClientRect();
          let firstStop = (imageBounding.height/3)*2;
          let secondStop = imageBounding.height + firstBounding.height + secondBounding.height + thirdBounding.height  + (lastBounding.height/3);
          if((window.pageYOffset > firstStop) && (window.pageYOffset < secondStop)) {
            let calculate = ((window.pageYOffset - firstStop)/(secondStop - firstStop))*100;
            document.getElementById('inner-line').style.height = `${calculate}%`;
            document.getElementById('side-nav').style.display = 'block';
          } else {
            document.getElementById('side-nav').style.display = 'none';
          }
        }
      }
    });
    if(screen.width < 768 &&  document.getElementById('index-content')) {

      document.getElementById('index-content').classList.add('index-content');
    } else if(document.getElementById('index-content')) {
      document.getElementById('index-content').classList.remove('index-content');
      document.getElementById('index-content').classList.remove('non-index-content');
    }
    if(screen.width < 768 && document.getElementById('home-menu')) {
      document.getElementById('home-menu').classList.add('header-home-menu');
    } else {
      document.getElementById('home-menu').classList.remove('header-home-menu');
      document.getElementById('home-menu').classList.remove('non-header-home-menu');
    }
    window.addEventListener("resize", function(){
      if(screen.width < 768 && document.getElementById('index-content')) {
        document.getElementById('index-content').classList.add('index-content');
      } else if(document.getElementById('index-content')) {
        document.getElementById('index-content').classList.remove('index-content');
        document.getElementById('index-content').classList.remove('non-index-content');
      }
      if(screen.width < 768 && document.getElementById('home-menu')) {
        document.getElementById('home-menu').classList.add('header-home-menu');
      } else {
        document.getElementById('home-menu').classList.remove('header-home-menu');
        document.getElementById('home-menu').classList.remove('non-header-home-menu');
      }
    });
  }
  render() {
    const { Component, pageProps, store } = this.props;
    return (
      <div>
        <Provider store={store}>
          <Component {...pageProps}/>
        </Provider>
      </div>
    )
  }
}

export default withRedux(createStore)(withReduxSaga(MyApp));





