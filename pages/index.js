import React, { useState } from 'react'
import Head from 'next/head'
import Link from 'next/link'
import { FullPage, Slide } from 'react-full-page';
import { Layout, Row, Col, Icon } from 'antd/lib';
import "react-responsive-carousel/lib/styles/carousel.min.css";
import { Carousel } from 'react-responsive-carousel';

const { Header, Footer, Sider, Content } = Layout;
const Index = () => {
  const [ cClass, setClass ] = useState('')
  const handleMenu = e => {

    e.preventDefault();

    const ele = document.getElementById('home-menu').style.display;
    if(ele && ele === 'block') {
      document.getElementById('home-menu').style.display = 'none';
      document.getElementById('index-content').classList.remove('non-index-content');
      document.getElementById('index-content').classList.add('index-content');
    } else {
      document.getElementById('home-menu').style.display = 'block';
      document.getElementById('index-content').classList.remove('index-content');
      document.getElementById('index-content').classList.add('non-index-content');
    }

  }
  return (
  <div>
    <Head>
      <title>Home</title>
      <link rel="icon" href="/favicon.ico" />
    </Head>
    <section className="image-scroller" id="image-scroller">
      <Row >
        <Col span={1}></Col>
        <Col span={22}>
          <div className="menu" style={{position: 'absolute', zIndex: 1, width: '100%'}}>
            <Row>
              <Col xs={12} sm={12} md={0} lg={0}><Link href="/"><span className="header-title">Cargo</span></Link></Col>
              <Col xs={12} sm={12} md={0} lg={0}  className="mobile-menu-icon"><Icon onClick={handleMenu} className="menu-icon" type="menu-fold" /></Col>
            </Row>
            <Row>
              <Col xs={0} sm={0} md={12} lg={12}><Link href="/"><span className="header-title">Cargo</span></Link></Col>

              <Col xs={24} sm={24} md={12} lg={12} >
                <Row type="flex" justify="end" id="home-menu" className="home-menu" >
                  <Col xs={24} sm={24} md={4} lg={4}  className="sub-menu"><Link href="/drive">Drive</Link></Col>
                  <Col xs={24} sm={24} md={4} lg={4}  className="sub-menu">Send</Col>
                  <Col xs={24} sm={24} md={4} lg={4}  className="sub-menu">Track</Col>
                  <Col xs={24} sm={24} md={4} lg={4}  className="sub-menu">Careers</Col>
                  <Col xs={24} sm={24} md={4} lg={4}  className="sub-menu"><Link href="/support">Support</Link></Col>
                  <Col xs={24} sm={24} md={4} lg={4}  className="sub-menu">Signup<Icon type="arrow-right" /></Col>
                </Row>
              </Col>
            </Row>
          </div>

          <Carousel className="img-carousel" autoPlay  showArrows={true} showThumbs={false} dynamicHeight={true} width={'100%'}>
            <div>
                <img src="/static/images/first.jpeg"/>
            </div>
            <div>
                <img src="/static/images/second.jpeg" />
            </div>
            <div>
                <img src="/static/images/first.jpeg" />
            </div>
          </Carousel>
        </Col>
        <Col span={1}></Col>
      </Row>
    </section>
    <section style={{position: 'absolute'}} id="index-content" className="">
      <Row>
        <Col xs={0} sm={0} md={5} >
          <nav id="side-nav" className="side-nav">
            <Row type="flex" justify="end" style={{height: '100%'}}>
              <Col span={1}>
                <div className="vertical-line">
                  <div id="inner-line"></div>
                </div>
              </Col>
              <Col span={23}>
                <ul className="menu-list">
                  <li><a href="#1">Who we are</a></li>
                  <li><a href="#2">Benefits</a></li>
                  <li><a href="#3">Ambassadors</a></li>
                  <li><a href="#4">Referral program</a></li>
                </ul>
              </Col>
            </Row>
          </nav>
        </Col>
        <Col sm={24} md={19}>
          <section>
            <FullPage>
              <Slide id="first" className="menu-item" style={{display: 'flex', alignItems: 'center', justifyContent: 'center'}}>
                <section className="second-slide">
                  <h1>
                    <p>
                      Common people who think & dream about saving our planet for future generations and minimize usage of resources today by providing effective ways of transporations for your logistic needs.
                    </p>
                  </h1>
                </section>
              </Slide>
              <Slide id="second" className="menu-item " style={{display: 'flex', alignItems: 'center', justifyContent: 'center', backgroundColor: '#F5F5F5'}}>
                <section className="second-slide">
                  <h1>
                    <p>
                      Producing lower Carbon footprint, Safe and effective ways of transorting your goods, generating extra income and Improving circular economy and most importantly saving the planet for future generations.
                    </p>
                  </h1>
                </section>
              </Slide>
              <Slide id="third"  className="menu-item " style={{display: 'flex', alignItems: 'center', justifyContent: 'center'}}>
                <section className="second-slide third">
                  <h1>
                    <p>
                      Every person using our platform are our ambassadors. We believe in action and combine the good actions to make business generating revenue for everyone.                    
                    </p>
                  </h1>
                </section>
              </Slide>
              <Slide id="forth" className="menu-item " style={{display: 'flex', alignItems: 'center', justifyContent: 'center', backgroundColor: '#F5F5F5'}}>
                <section className="second-slide">
                  <h1>
                    <p>
                      We have one agenda with our referral program and we are not using any modern marketing tools to get the word out, because we believe what our ambassadors believe.For every referral signup, our ambassadors get some credit points which they can use for their logistic needs.
                    </p>
                  </h1>
                </section>
              </Slide>
              <Slide className="video-carl">
              <section id="video-carousel" className="video-carousel">
                  <Carousel  autoPlay  showArrows={true} showThumbs={false} dynamicHeight={true} width={'99%'}>
                    <div>
                      <video width="100%" height="480" controls>
                        <source src="/static/images/first.mp4" type="video/mp4" />
                      </video>
                      </div>
                    <div>
                      <video width="100%" height="480" controls>
                        <source src="/static/images/second.mp4" type="video/mp4" />
                      </video>
                    </div>
                  </Carousel>
              </section>
              <section className="home-footer">
                <div className="footer">
                  <Row  style={{marginBottom: '5%'}}>
                    <Col sm={8} lg={6} className="footer-sub">
                      <ul className="footer-menu">
                        <li className="footer-head"><span>Resources</span></li>
                        <li className="footer-list"><span>Training Materials</span></li>
                      </ul>
                    </Col>
                    <Col sm={8} lg={6} className="footer-sub">
                      <ul className="footer-menu">
                        <li className="footer-head"><span>Company</span></li>
                        <li className="footer-list"><span>Careers</span></li>
                        <li className="footer-list"><span>Blog</span></li>
                      </ul>
                    </Col>
                    <Col sm={8} lg={6} className="footer-sub">
                      <ul className="footer-menu">
                        <li className="footer-head"><Link href="/aboutUs"><span>About Us</span></Link></li>
                        <li className="footer-list"><Link href="/contact"><span>Contact</span></Link></li>
                        <li className="footer-list"><span>Press</span></li>
                      </ul>
                    </Col>
                    <Col sm={24} lg={6} className="footer-social">
                      <span className="footer-head">Get the app</span>
                      <div className="app-store">
                        <div style={{textAlign: 'center'}}>
                          <div className="app-icon">
                            <Icon type="apple" />
                          </div>
                          <div>Get it on</div>
                          <div className="app-header">App Store</div>
                        </div>
                        <div style={{marginLeft: '10%', textAlign: 'center'}}>
                          <div className="app-icon">
                            <Icon type="android" />
                          </div>
                          <div>Get it on</div>
                          <div className="app-header">Google Play</div>
                        </div>
                      </div>
                    </Col>
                  </Row>
                  <hr />
                  <div style={{display: 'flex', flexWrap: 'nowrap', marginTop: 33}}>
                    <div > <span className="media"><Icon type="facebook" /></span></div>
                    <div > <span className="media"><Icon type="twitter" /></span></div>
                    <div > <span className="media"><Icon type="instagram" /></span></div>
                    <div > <span className="media"><Icon type="linkedin" /></span></div>
                    <div  className="copyright"> <span>All rights reserved</span></div>
                  </div>
                </div>
              </section>
              </Slide>
            </FullPage>
          </section>
        </Col>
      </Row>
    </section>
    
  </div>
)
}
export default Index
