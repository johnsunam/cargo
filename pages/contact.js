import React from 'react';
import { Row, Col, Input, Button, Card} from 'antd/lib';
import CommonLayout from '../components/layout';

const Contact =  (props) => {
  return (
    <CommonLayout>
      <Row style={{marginBottom: '10%'}}>
        <Col xs={0} sm={0} md={3} lg={3}></Col>
        <Col xs={24} sm={24} md={18} lg={18}>
          <h1 className="contact-header">Get in touch</h1>
          <Row style={{marginTop: '2%'}}>
            <Col xs={24} sm={24} md={12} lg={12}>
              <Card style={{ width: '90%' }} className="contact-card-first">
                <div className="card-title">Carrier's Support</div>
                <div className="card-mail">cargo-carrier@cargo.com</div>
              </Card>
            </Col>
            <Col xs={24} sm={24} md={12} lg={12}>
              <Card style={{ width: '90%' }} className="contact-card-second">
                <div className="card-title">Carrier's Support</div>
                <div className="card-mail">cargo-carrier@cargo.com</div>
              </Card>
            </Col>
          </Row>
          <Row style={{marginTop: '2%'}}>
            <Col xs={24} sm={24} md={12} lg={12}>
              <Card style={{ width: '90%' }} className="contact-card-third">
                <div className="card-title">Carrier's Support</div>
                <div className="card-mail">cargo-carrier@cargo.com</div>
              </Card>
            </Col>
            <Col xs={24} sm={24} md={12} lg={12}>
              <Card style={{ width: '90%' }} className="contact-card-forth">
                <div className="card-title">Carrier's Support</div>
                <div className="card-mail">cargo-carrier@cargo.com</div>
              </Card>
            </Col>
          </Row>
          <Row style={{marginTop: '2%'}}>
            <Col xs={24} sm={24} md={23} lg={23}>
              <Card style={{ width: '99%' }} className="contact-card-fifth">
                <div className="card-title">Press Kit</div>
                <div className="card-mail">Download media asset</div>
              </Card>
            </Col>
          </Row>
        </Col>
        <Col xs={0} sm={0} md={3} lg={3}></Col>
      </Row>
      <Row style={{marginBottom: '5%'}}>
        <Col xs={0} sm={0} md={1} lg={1} ></Col>
        <Col span={22} style={{background: '#e8e8e8'}}>
          <div style={{textAlign: 'center', margin: '9%'}}>
            <div style={{fontSize: '3.5rem'}}>Work with us</div>
            <Button type="primary" shape="round" size={'large'} style={{height: '4rem', width: '14rem'}}>
              View job openings
            </Button>
          </div>
        </Col>
        <Col xs={0} sm={0} md={1} lg={1} ></Col>
      </Row>
    </CommonLayout>
  )
}
export default Contact;