import React, { useState } from 'react';
import { Row, Col, Button, Steps, Divider  } from 'antd/lib';
import { Carousel } from 'react-responsive-carousel';
import "react-responsive-carousel/lib/styles/carousel.min.css";
import CommonLayout from '../components/layout';
const { Step } = Steps;
const descriptions = [
    "Signup and enter your details ( We adhere to GDPR and promise you that your data is saffe with us)",
    "Upload required documents and vehicle details",
    "Wait until your Documents are validated by us.",
    "Once validated, cross check all the details you have entered.",
    "You are live, you can manage your fleets availability and make business based on the availability of your vehicle."
]
const Drive = props => {
  const [stepNum, setStep] = useState(0);

  const onChange = current => {
    console.log('onChange:', current);
    setStep(current);
  };

  return (
    <CommonLayout>
      <Row type="flex" justify="center">
        <Col xs={0} sm={0} md={22} lg={22}>
          <Carousel className="img-carousel"   showArrows={true} showThumbs={false} dynamicHeight={true} width={'100%'} >
            <div>
              <img src="/static/images/first.jpeg" width="100%"/>
              <p className="drive-img">
                We know it takes courage to drive someone else's goods, but dont you worry we are here to make it safe for you.
              </p>
            </div>
            <div>
              <img src="/static/images/first.jpeg" width="100%"/>
              <p className="drive-img">
                You can earn little extra cash, good Karma and make
                your commute useful for others while saving the planet for future generations.
              </p>
            </div>
          </Carousel>
        </Col>
      </Row>
      <Row type="flex" align="middle" justify="center" className="drive-mid">
        <Col xs={24} sm={24} md={11} lg={11} className="drive-div">
          <Button className="drive-login" type="primary" shape="round"  size="large">
            Login
          </Button>
        </Col>
        <Col xs={24} sm={24} md={11} lg={11} className="drive-div">
          <div className="signup-block">
            <Button className="drive-signup" type="primary" shape="round"  size="large">
              Get started
            </Button> 
            <p>Too early or want to know more?</p>
          </div>
        </Col>
      </Row>
      <Row type="flex" justify="center" className="how">
        <Col span={10} className="drive-how">How it works</Col>
      </Row>
      <Row type="flex" justify="center" className="drive-step">
        <Col xs={0} sm={0} md={5} lg={5} className="how-step">
          <Steps current={stepNum} onChange={onChange} direction="vertical">
          <Step title="Step 1"/>
          <Step title="Step 2"/>
          <Step title="Step 3"/>
          <Step title="Step 4"/>
          <Step title="Step 5"/>
        </Steps>
        </Col>
        <Col key={stepNum} xs={0} sm={0} md={17} lg={17} className="how-content">
          <h1>Step: {stepNum + 1}</h1>
          <hr />
          <p>
            {descriptions[stepNum]}
          </p>
        </Col>
        {descriptions.map((content, i) => <Col key={i} xs={24} sm={24} md={0} lg={0} className="how-content">
          <h1>Step: {i + 1}</h1>
          <hr />
          <p>
            {descriptions[i]}
          </p>
        </Col>)}
      </Row>
    </CommonLayout>
  )
}

export default Drive;