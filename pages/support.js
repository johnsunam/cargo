import React from 'react';
import Link from 'next/link';
import CommonLayout from '../components/layout';
import { Row, Col, Input, Card, Pagination, Button } from 'antd/lib';

const { Search } = Input;

const Support = props => {
  return(
    <CommonLayout>
      <Row>
        <Col xs={0} sm={0} md={3} lg={3}></Col>
        <Col xs={24} sm={24} md={18} lg={18} className="suppport-header">
          Support content
        </Col>
        <Col xs={0} sm={0} md={3} lg={3}>
        </Col>
      </Row>
      <div className="search">
        <Search size="large" placeholder="What are you looking for ?" loading={false} enterButton />
      </div>
      <Row className="search-cat">
        <Col xs={0} sm={0} md={8} lg={8}></Col>
        <Col xs={24} sm={24} md={8} lg={8}>
          <Row type="flex" justify="center" className="search-category">
            <Col span={8} >Carrier</Col>
            <Col span={8} className="cat">Sender</Col>
            <Col span={8} className="cat">All</Col>
          </Row>
        </Col>
        <Col xs={0} sm={0} md={8} lg={8}></Col>
      </Row>
      <Row type="flex" justify="space-around" className="question">
        <Col xs={24} sm={24} md={7} lg={7} className="support-block">
          <Link href="/contact">
            <Card style={{ width: "100%" }}>
              <p className="title">Card content</p>
              <p className="que-content">How do we get started with cargo?</p>
            </Card>
          </Link>
        </Col>
        <Col xs={24} sm={24} md={7} lg={7} className="support-block">
          <Link href="/contact">
            <Card style={{ width: "100%" }}>
              <p className="title">Card content</p>
              <p className="que-content">How do we get started with cargo?</p>
            </Card>
          </Link>
        </Col>
        <Col xs={24} sm={24} md={7} lg={7} className="support-block">
          <Link href="/contact">
            <Card style={{ width: "100%" }}>
              <p className="title">Card content</p>
              <p className="que-content">How do we get started with cargo?</p>
            </Card>
          </Link>
        </Col>
        <Col xs={24} sm={24} md={7} lg={7} className="support-block">
          <Link href="/contact">
            <Card style={{ width: "100%" }}>
              <p className="title">Card content</p>
              <p className="que-content">How do we get started with cargo?</p>
            </Card>
          </Link>
        </Col>
        <Col xs={24} sm={24} md={7} lg={7} className="support-block">
          <Link href="/contact">
            <Card style={{ width: "100%" }}>
              <p className="title">Card content</p>
              <p className="que-content">How do we get started with cargo?</p>
            </Card>
          </Link>
        </Col>
        <Col xs={24} sm={24} md={7} lg={7} className="support-block">
          <Link href="/contact">
            <Card style={{ width: "100%" }}>
              <p className="title">Card content</p>
              <p className="que-content">How do we get started with cargo?</p>
            </Card>
          </Link>
        </Col>
      </Row>
      <div className="support-pag">
        <Pagination defaultCurrent={1} total={50} />
      </div>
      <Row className="contact-block" type="flex" justify="center">
        <Col span={20} className="contact-content">
          <h1>Still need help?</h1>
          <Button type="primary" shape="round" size="large">
            Contact us
        </Button>
        </Col>
      </Row>
    </CommonLayout>
  )
}

export default Support;